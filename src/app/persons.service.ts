import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonsService {

  constructor(private httpClient : HttpClient) { }

  public getJSON(url): Observable<any> {
    return this.httpClient.get(url);
  }

}
