import { Component} from '@angular/core';
import { PersonsService } from '../persons.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {

  private URL : string = 'assets/persons.json';
  items : any;
 
  constructor(private personsService : PersonsService) {

    personsService.getJSON(this.URL).subscribe(data => {
      
      //console.log(data);
      this.items = data;
     });
  }

}
